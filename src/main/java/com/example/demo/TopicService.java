package com.example.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicService {

    @Autowired
    private TopicRepository topicRepository;

    private List<Topic> topics = new ArrayList<>(
            Arrays.asList(new Topic("spring", "Spring Framework", "Spring Frameworke Description"),
                    new Topic("java", "core java", "core java Description"),
                    new Topic("javascript", "javascript language", "javascript language Description")));

    public List<Topic> getTopics() {
        List<Topic> res = new ArrayList<>();
        topicRepository.findAll().forEach(res::add);
        return res;
    }

    public Topic getTopic(String id) {
        return this.topics.stream().filter((topic) -> topic.getId().equals(id)).findFirst().get();
    }

    public void addTopic(Topic topic) {
        this.topics.add(topic);
    }

    public void updateTopic(Topic topic, String id) {
        this.topics.removeIf((t) -> t.getId().equals(id));
        this.topics.add(topic);
    }

    public void deleteTopic(String id) {
        this.topics.removeIf((t) -> t.getId().equals(id));
    }
}
