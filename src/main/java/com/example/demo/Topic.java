package com.example.demo;

import javax.persistence.Entity;

@Entity
public class Topic {
    private final String id;
    private final String content;
    private final String description;

    public Topic(String id, String content, String description) {
        this.id = id;
        this.content = content;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public String getDescription() {
        return description;
    }
}
